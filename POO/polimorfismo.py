#Habilidad de tener diferentes comportamientos basado en que subclase se esta utilizando
#relacionado muy estrechamente con herencia

#CLASE PADRE
class Restaurant:
    def __init__(self,nombre,categoria,precio):
        self.nombre=nombre
        self.categoria = categoria
        self.precio = precio

    def mostar_informacion(self):
        print(f'Restaurant: {self.nombre}, '
              f'Categoria: {self.categoria}, '
              f'Precio: {self.precio}')


#CLASE HIJO
class Hotel(Restaurant):
    #agregando polimorfismo en init con banderas
    def __init__(self,nombre,categoria,precio,banderas):
        super().__init__(nombre,categoria,precio)
        #agregando lo específico de la clase hotel
        self.banderas=banderas

    #agregando un método de hotel. parte de polimorfismo
    def get_banderas(self):
        return self.banderas

    #reescribiendo un método de la clase padre. parte de polimorfismo
    #para reescribir el método debe de tener el mismo nombre
    def mostar_informacion(self):
        print(f'Restaurant: {self.nombre}, '
              f'Categoria: {self.categoria}, '
              f'Precio: {self.precio}',
              f'Banderas: {self.banderas}')

#instanciando la clase hija
hotel=Hotel('Casa blanca','Tres estrellas',2500,'5 banderas')
hotel.mostar_informacion()
banderas=hotel.get_banderas()
print(f"Solo de la clase hotel: {banderas}")