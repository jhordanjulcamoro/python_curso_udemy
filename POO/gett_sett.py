class Restaurant:
    def __init__(self,nombre,categoria,precio):
        self.nombre=nombre
        self.categoria = categoria
        self.__precio = precio

    def mostar_rest(self):
        print(f'Restaurant: {self.nombre}, '
              f'Categoria: {self.categoria}, '
              f'Precio: {self.__precio}')

    #GETTERS Y SETTERS
    #GET =  OBTIENE UN VALOR
    def get_precio(selfs):
        return selfs.__precio
    #SET = ASIGNA/AGREGA UN VALOR
    def set_precio(self,precio):
        self.__precio=precio


restaurant=Restaurant('Las delicias','Comida Italiana',500)
restaurant.set_precio(120)
precio=restaurant.get_precio()
print(precio)
restaurant.mostar_rest()

restaurant2=Restaurant('Los delfines','Cevichería',200)
restaurant2.set_precio(300)
precio2=restaurant2.get_precio()
print(precio2)
restaurant2.mostar_rest()
