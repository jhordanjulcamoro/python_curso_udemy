class Restaurant:
    def __init__(self,nombre,categoria,precio):
        self.nombre=nombre #Este atributo esta por default en public
        self.categoria = categoria
        self._precio = precio #Este atributo esta en PROTECTED
        #self.__precio=precio #Este atributo esta en PRIVATE

    #PARA PROTECTED SE PONE UN GUIÓN BAJO
    # PARA PROTECTED SE PONE DOS GUIONES BAJO

    def mostar_rest(self):
        print(f'Restaurant: {self.__nombre}, '
              f'Categoria: {self.__categoria}, '
              f'Precio: {self.__precio}')


#Instancia 1
restaurant=Restaurant('Las delicias','Comida Italiana',500)
restaurant.mostar_rest()
#Instancia 2
restaurant2=Restaurant('Los delfines','Cevichería',200)
restaurant2.mostar_rest()
