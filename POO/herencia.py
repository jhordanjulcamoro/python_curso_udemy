#Al heredar una clase tendrás todos los métodos y atributos de la
#clase padre en el hijo

#CLASE PADRE
class Restaurant:
    def __init__(self,nombre,categoria,precio):
        self.nombre=nombre
        self.categoria = categoria
        self.__precio = precio

    def mostar_informacion(self):
        print(f'Restaurant: {self.nombre}, '
              f'Categoria: {self.categoria}, '
              f'Precio: {self.__precio}')

    def get_precio(selfs):
        return selfs.__precio

    def set_precio(self,precio):
        self.__precio=precio


#CLASE HIJO
class Hotel(Restaurant):
    def __init__(self,nombre,categoria,precio):
        super().__init__(nombre,categoria,precio)

#instanciando la clase hija
hotel=Hotel('Casa blanca','Tres estrellas',2500)
hotel.mostar_informacion()