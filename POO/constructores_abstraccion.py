class Restaurant:
    #este es un constructor y sirve para ingresar los elementos
    #que serán utilizables desde el principio
    def __init__(self,nombre,categoria,precio):
        self.nombre=nombre
        self.categoria = categoria
        self.precio = precio
        #Se aplica abstracción en estos elementos debido a que solo
        #se solicitan los elementos necesario para esta clase y de igual manera los necesarios para le objeto
        #los innecesarios como el color del plato no

    def mostar_rest(self):
        print(f'Restaurant: {self.nombre}, '
              f'Categoria: {self.categoria}, '
              f'Precio: {self.precio}')

#al instanciar, los datos se envían directo al constructor que
#hemos creado debido a que se inicializa solicitando los mismos
restaurant=Restaurant('Las delicias','Comida Italiana',500)
restaurant.mostar_rest()

restaurant2=Restaurant('Los delfines','Cevichería',200)
restaurant2.mostar_rest()

restaurant3=Restaurant('Los tiburones','Comida Mexicana',58)
restaurant3.mostar_rest()