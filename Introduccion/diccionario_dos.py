#creando diccionario vacio
jugador={}
print(jugador)

#se une un jugador
jugador['nombre']='Jhordan'
jugador['puntaje']=0
print(jugador)

#se modifica su puntaje
jugador['puntaje']=100
print(jugador)

#se soliita el puntaje del jugador
#esto por medio de una petición
puntaje_actualizado= jugador.get('puntaje')
print(f'el puntaje es {puntaje_actualizado}')

#se solicita un dato que aún no existe en el diccionario
#se ingresa un valor por defecto si no existe el dato
print(jugador.get('sala','Aún no se cuenta con una sala'))

#iterar el diccionario
for llave, valor in jugador.items():
    print(f'LLave {llave} y valor {valor}')
    print(llave)
    print(valor)

#eliminar un dato
del jugador['nombre']
del jugador['puntaje']
print(jugador)