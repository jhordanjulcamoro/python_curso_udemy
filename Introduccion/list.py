lista_carros=["mazda","audi","bmw","kia"]
print(lista_carros)

#Sirve para agregar un elemento
lista_carros.append("audi")
print(lista_carros)

#para borrar un elemento de una posicion indica en la lista
del lista_carros[4]
print(lista_carros)

#elimina el ultimo elemento de la lista o de un arreglo
lista_carros.pop()
print(lista_carros)

#eliminar por nombre
lista_carros.remove("audi")
print(lista_carros)