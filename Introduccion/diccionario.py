creacion = {
    'artista': 'Metalica',
    'cancion': 'The roums',
    'lanzamiento': 1997,
    'likes': 4500
}

#imprimiendo diccionario
print(creacion)
#imprimiento parámetro artista
print(creacion['artista'])

#mezclar elementos con un string
musica=creacion['artista']
print(f'Estoy escuchando {musica}')

#agregando valores
creacion['playlist']='los cucos'
creacion['feats']='los cerditos'
print(creacion)

#reemplazando valores
creacion['feats']='los tres cerditos'
print(creacion)