#Ejercicio para crear una playlist

playlist = {} #diccionario vacio
playlist['canciones'] = [] #Lista vacia de nombre de canciones

#creando una funcion principal
def app():

    agregar_playlist=True

    while agregar_playlist:
        nombre_playlist=input('Como se llamará su playlist?\r\n')

        if nombre_playlist:
            playlist['nombre']=nombre_playlist

            #el playlist ya tiene nombre
            agregar_playlist=False

            #mandamos llegar la función para agregar canciones
            agregar_canciones()


def agregar_canciones():
    #bandera para cancion, bool
    agregar_canciones=True

    while agregar_canciones:
        nombre_playlist=playlist['nombre']
        pregunta=f'\r\nAgrega canciones para la playlist {nombre_playlist}: \r\n'
        pregunta+= 'Escribe "X para dejar de agregar canciones\r\n'
        cancion=input(pregunta)

        if cancion=='X':
            #Dejar de agregar canciones
            agregar_canciones=False
            resumen_playlist()
        else:
            #Agregar las canciones a la playlist
            playlist['canciones'].append(cancion)
            #print('Lista de canciones: ',playlist['canciones'])

def resumen_playlist():
    print(f'Resumen de tus canciones')
    print(f'Playlist: {playlist["nombre"]}')
    print(f'Canciones agregadas:')
    for cancion in playlist["canciones"]:
        print(cancion)
app()