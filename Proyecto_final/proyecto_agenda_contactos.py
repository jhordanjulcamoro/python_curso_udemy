#AGENDA DE CONTACTOS, DÓNDE CADA CONTACTO SE GUARDARÁ EN UN ARCHIVO

#IMPORTAMOS UNA LIBRERÍA PARA CREAR CARPETAS
import os
#Creamos una constante para almacenar la extensión de la carpeta
CARPETA='contactos/'
EXTENSION='.txt'

#Creamos una clase para nuestro contacto
class Contacto:
    def __init__(self,nombre,numero,categoria):
        self.nombre=nombre
        self.numero=numero
        self.categoria=categoria
#definimos la funcion principal del app
def app():
    #creamos una funcion para revisar si existe la carpeta, sino la crea
    crear_directorio()
    #creamos una variable para la opcion
    preguntar_menu=True
    while preguntar_menu:
        # creamos un print de las opciones del menú
        menu()
        opcion_menu=int(input("Seleccione opción: "))
        #Opciones del menú
        if opcion_menu==1:
            agregar_contacto()
        elif opcion_menu==2:
            editar_contacto()
        elif opcion_menu == 3:
            ver_contacto()
        elif opcion_menu == 4:
            buscar_contacto()
        elif opcion_menu == 5:
            eliminar_contacto()
        elif opcion_menu == 6:
            print('El programa esta finalizando . . .')
            preguntar_menu = False
        else:
            print('*opción invalida, intente nuevamente*')


def agregar_contacto():
    #Ingresamos el nombre para poder generar nuestra carpeta
    nombre_contacto=input('Ingrese el nombre: \r\n')

    #Validar si el archivo ya existe
    carpeta_existe=funcion_existe_contacto(nombre_contacto)
    if not carpeta_existe:
        #Escribimos en la carpeta
        with open (CARPETA+nombre_contacto+EXTENSION,'w') as archivo:
            #recibimos el número y la categoria
            numero_contacto = int(input('Ingrese el número: \r\n'))
            categoria_contacto=input("Ingrese la categoría: \r\n")

            contacto=Contacto(nombre_contacto,numero_contacto,categoria_contacto)
            archivo.write('Contacto: '+contacto.nombre+'\r\n'  +
                          'Numero: '+ str(contacto.numero)+ '\r\n' +
                          'Categoria: '+ contacto.categoria + '\r\n' )

            print('*****  Registro exitoso  *****')
    else:
        print("El contacto ya existe!!!")

def editar_contacto():
    nombre_contacto_edit=input('Ingrese el nombre del contacto a editar: \r\n')

    # Validar si el archivo ya existe
    carpeta_existe = funcion_existe_contacto(nombre_contacto_edit)

    if carpeta_existe:
        #leer carpeta a editar
        with open(CARPETA+nombre_contacto_edit+EXTENSION,'w') as archivo:
            # recibimos los datos a editar
            nuevo_nombre=input("Ingresa el nuevo nombre: \r\n")
            nuevo_numero = int(input('Ingrese el nuevo número: \r\n'))
            nuevo_categoria= input("Ingrese la nueva categoría: \r\n")

            #Instanciar
            contacto_nuevo=Contacto(nuevo_nombre,nuevo_numero,nuevo_categoria)

            #Escribir en el archivo
            archivo.write('Contacto: ' + nuevo_nombre + '\r\n' +
                          'Numero: ' + str(nuevo_numero) + '\r\n' +
                          'Categoria: ' + nuevo_categoria + '\r\n')

            print('Editando . . .')

        # modificamos el nombre de la carpeta
        os.rename(CARPETA + nombre_contacto_edit + EXTENSION, CARPETA + nuevo_nombre + EXTENSION)
        print("Editado exitosamente!!!")
    else:
        print("No podemos editar el contacto debido a que no existe!!!")

def ver_contacto():
    #accediendo a la carpeta archivo
    archivos=os.listdir(CARPETA)
    #indicando que solo lea los tipos txt
    archivos_txt=[i for i in archivos if i.endswith(EXTENSION)]

    #leendo todos los contactos y sus contenidos
    for archivo in archivos_txt:
        #accediendo a los datos de cada carpeta/contacto
        with open(CARPETA+archivo) as contacto:
            #recorriendo los datos internos
            for linea in contacto:
                print(linea.rstrip())
            print('\r\n')

def buscar_contacto():
    nombre_contacto_leer = input("Ingrese el nombre del contacto a leer: \r\n")
    try:
        # abrimos la carpeta
        with open(CARPETA + nombre_contacto_leer + EXTENSION) as archivo:
            print(f"Los datos de {nombre_contacto_leer} son: \r\n")
            for contenido in archivo:
                print(contenido.rstrip())
    except IOError:
        print("El contacto ingresado no existe!!!")
        print("Error:" + IOError)
    """OTRA MANERA
     # comprobamos que la carpeta exista
    carpeta_existe = funcion_existe_contacto(nombre_contacto_leer)
    if carpeta_existe:
        # abrimos la carpeta
        with open(CARPETA + nombre_contacto_leer + EXTENSION) as archivo:
            print(f"Los datos de {nombre_contacto_leer} son: \r\n")
            for contenido in archivo:
                print(contenido.rstrip())
    else:
        print("El contacto ingresado no existe!!!")
    """

def eliminar_contacto():
    #ingresamos el nombre del contacto a eliminar
    nombre_contacto_eliminar = input("Ingrese el contacto a eliminar: ")

    try:
        os.remove(CARPETA+nombre_contacto_eliminar+EXTENSION)
        print("El contacto fue eliminado exitosamente :D")
    except IOError:
        print("El contacto no puede ser eliminado debido a que no existe!!!")

    """OTRA MANERA DE HACERLO
    #verificamos si el contacto existe
    contacto_existe=funcion_existe_contacto(nombre_contacto_eliminar)
    if contacto_existe:
        os.remove(CARPETA+nombre_contacto_eliminar+EXTENSION)
        print("El contacto fue eliminado exitosamente :D")
    else:
        print("El contacto no puede ser eliminado debido a que no existe!!!")
    """
def funcion_existe_contacto(nombre_carpeta):
    return os.path.isfile(CARPETA+nombre_carpeta+EXTENSION)

def menu():
    print('**********MENU***********')
    print('[1] Agregar nuevo contacto')
    print('[2] Editar contacto')
    print('[3] Listar contactos')
    print('[4] Buscar contacto')
    print('[5] Eliminar contacto')
    print('[6] Salir')

def crear_directorio():
    #Si no existe la extención entonces crearla
    if not os.path.exists(CARPETA):
        #crear la carpeta
        os.makedirs(CARPETA)


#INICIO DE LA APLICACIÓN
app()