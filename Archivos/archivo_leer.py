def app():
    #con open estamos abriendo el archivo
    with open('archivo_creado.txt') as archivo:
        #con el for estamos recorriendo su contenido
        for contenido in archivo:
            #imprimimos el contenido
            ##print(contenido)
            #como tiene saltos de línea, las borramos así:
            print(contenido.rstrip())

app()