def app():
    #Crear el archivo
    archivo=open('archivo_creado.txt','w') #w es permiso de escritura, si no existe, se creará

    #Generar numeros en archivos
    for numero in range(1,20):
        archivo.write("Esta es la escritura "+str(numero)+"\r\n")
    #Cerrar el archivo
    archivo.close()

app()